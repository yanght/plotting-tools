#ifndef __ROOTFILE_H
#define __ROOTFILE_H

#include <memory>

#include <TFile.h>
#include <TDirectory.h>
#include <TObject.h>
#include <TH1.h>
#include <json.hpp>

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

class RootFile
{
public:
    /// Constructor
    RootFile();

    /// Deconstructor
    ~RootFile();

    /// Function to recreate/load ROOT file
    void recreate(std::string /*i_path*/);
    void recreate(std::string /*i_path*/,
                  std::string /*i_dir*/);
    int load(std::string /*i_path*/);

    /// Function to set parameters for plotting
    void setPrint(std::string /*i_ext*/,
                  std::string /*o_dir*/);

    /// Function to write data into ROOT file
    int writeScanLog(std::string /*i_dir*/);
    int writeScanCfg(std::string /*i_dir*/);
    int writeChipCfg(std::string /*i_dir*/,
                     int /*i_num*/);
    int writeChipData(std::string /*i_dir*/,
                      int /*i_num*/);

    /// Function to load/replicate data in ROOT file
    int loadScanLog();
    int loadScanCfg();
    int loadChipData();

    /// Function to get parameters
    int getChips();

private:
    void writeChipPlot(std::string /*i_name*/,
                       json&       /*i_data*/,
                       TDirectory* /*i_directory*/);
    void writeChipPlot(std::string /*i_name*/,
                       TObject*    /*i_o*/,
                       TDirectory* /*i_directory*/);
    void writeChipPlotProjection(std::string /*i_name*/,
                                 TObject*    /*i_o*/,
                                 TDirectory* /*i_directory*/);
    void writeChipPlotOccupancy(std::string /*i_name*/,
                                TObject*    /*i_o*/,
                                TDirectory* /*i_directory*/);
    void writeChipPlotNoiseOccupancy(std::string /*i_name*/,
                                     TObject*    /*i_o*/,
                                     TDirectory* /*i_directory*/);
    void writeChipPlotRms(std::string /*i_name*/,
                          TObject*    /*i_o*/,
                          TDirectory* /*i_directory*/);
    void writeChipPlotGaus(std::string /*i_name*/,
                           TObject*    /*i_o*/,
                           TDirectory* /*i_directory*/);

    TDirectory* mkdir(TFile*      /*i_mother*/,
                      std::string /*i_name*/);
    TDirectory* mkdir(TDirectory* /*i_mother*/,
                      std::string /*i_name*/);

    TFile* m_file;
    TFile* m_infile;
    bool m_output;
    bool m_input;

    std::string m_testtype;
    std::string m_chiptype;
    json m_chips;

    bool m_print;
    std::string m_ext;
    std::string m_dir;

    TH1* m_h;
};

#endif //__ROOTFILE_H
