#include <glob.h>
#include <time.h>

#include "RootFile.h"
#include "Env.h"
#include "ReadFile.h"
#include "PlotTool.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "HistoProjection.h"
#include "HistoGaus.h"
#include "HistoRms.h"
#include "HistoOccupancy.h"
#include "HistoNoiseOccupancy.h"

//////////////
/// Public ///
//////////////

RootFile::RootFile():
m_file(NULL), m_infile(NULL), m_output(false), m_input(false),
m_testtype(""), m_chiptype(""), m_chips(NULL),
m_print(false), m_ext(""), m_dir(""), m_h(NULL)
{
#ifdef DEBUG
    std::cout << "RootFile::RootFile()" << std::endl;
#endif
}

RootFile::~RootFile() {
#ifdef DEBUG
    std::cout << "RootFile::~RootFile()" << std::endl;
#endif
    if (m_input) {
        std::string path_clone = m_infile->GetName();
        m_infile->Close();
        delete m_infile;
        std::string cmd = "rm " + path_clone;
        if (system(cmd.c_str())<0) {
            std::cerr << "[WARNING] Could not remove " << path_clone << std::endl;
        }
        for (auto iterator_f : *m_file->GetListOfKeys()) {
            std::string chip_name = iterator_f->GetName();
            if (!m_file->Get(chip_name.c_str())->IsFolder()) continue;
            TCanvas* c = new TCanvas("c","", 800, 600);
            std::string path = m_dir + "/" + chip_name + ".pdf";
            c->Print((path+"]").c_str(), "pdf");
            delete c;
        }
    }
    if (m_output) {
        m_file->Close();
        delete m_file;
    }
}

void RootFile::recreate(std::string i_path) {
#ifdef DEBUG
    std::cout << "RootFile::recreate(" << i_path << ")" << std::endl;
#endif
    m_file = new TFile(i_path.c_str(), "RECREATE");
    m_output = true;
}

void RootFile::recreate(std::string i_path, std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::recreate(" << i_path << ", " << i_dir << ")" << std::endl;
#endif
    m_file = new TFile(i_path.c_str(), "RECREATE");
    m_output = true;
    m_dir = i_dir;

    for (auto iterator_f : *m_infile->GetListOfKeys()) {
        std::string chip_name = iterator_f->GetName();
        if (!m_infile->Get(chip_name.c_str())->IsFolder()) continue;
        TCanvas* c = new TCanvas("c","", 800, 600);
        std::string path = m_dir + "/" + chip_name + ".pdf";
        c->Print((path+"[").c_str(), "pdf");
        delete c;
    }
}

int RootFile::load(std::string i_path) {
#ifdef DEBUG
    std::cout << "RootFile::load(" << i_path << ")" << std::endl;
#endif
    struct stat filestat;
    if (stat(i_path.c_str(), &filestat)) {
        std::cerr << "[ERROR] Not found file at " << i_path << std::endl;
        return -1;
    }

    time_t t = time(NULL);
    std::string path_clone = i_path + std::to_string((int)t) + ".root";
    std::string cmd = "cp " + i_path + " " + path_clone;
    if (system(cmd.c_str())<0) {
        std::cerr << "[ERROR] Could not clone " << i_path << " to " << path_clone << std::endl;
        return -1;
    }
    m_infile = new TFile(path_clone.c_str());
    m_infile->SetName(path_clone.c_str());
    m_input = true;

    return 0;
}

void RootFile::setPrint(std::string i_ext, std::string o_dir) {
#ifdef DEBUG
    std::cout << "RootFile::setPrint(" << i_ext << ", " << o_dir << ")" << std::endl;
#endif
    m_print = true;
    m_ext = i_ext;
}

int RootFile::writeScanLog(std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::writeScanLog(" << i_dir << ")" << std::endl;
#endif
    struct stat filestat;
    std::string path = i_dir + "/scanLog.json";
    if (stat(path.c_str(), &filestat)) {
        std::cerr << "[ERROR] Not found scanLog at " << path << std::endl;
        return -1;
    }
    Env* e = new Env("scanLog", path);
    json j = e->getJson();
    if (!j["testType"].is_null()) {
        m_testtype = j["testType"];
    } else {
        std::cerr << "[ERROR] Could not read 'testType' from " << path << std::endl;
        return -1;
    }
    if (!j["chipType"].is_null()) {
        m_chiptype = j["chipType"];
    } else {
        std::cerr << "[ERROR] Could not read 'chipType' from " << path << std::endl;
        return -1;
    }
    if (!j["chips"].is_null()) {
        m_chips = j["chips"];
    } else {
        std::cerr << "[ERROR] Could not read 'chips' from " << path << std::endl;
        return -1;
    }
    m_file->cd();
    e->write();
    delete e;
    return 0;
}

int RootFile::writeScanCfg(std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::writeScanCfg(" << i_dir << ")" << std::endl;
#endif
    struct stat filestat;
    std::string path = i_dir + "/" + m_testtype + ".json";
    if (!stat(path.c_str(), &filestat)) {
        Env* e = new Env("scanCfg", path);
        m_file->cd();
        e->write();
        delete e;
    } else {
        std::cout << "[WARNING] Not found ScanCfg at " << path << std::endl;
    }
    return 0;
}

int RootFile::writeChipCfg(std::string i_dir, int i_num) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipCfg(" << i_dir << ", " << i_num << ")" << std::endl;
#endif
    TDirectory* d { nullptr };
    std::string chip_name = "DisabledChip_" + std::to_string(i_num);
    if (m_chips[i_num]["enable"].is_null()||m_chips[i_num]["enable"]!=0) {
        std::string cfg_path;
        if (!m_chips[i_num]["config"].is_null()) {
            cfg_path = m_chips[i_num]["config"];
        } else {
            std::cerr << "[ERROR] Could not read 'chips." << std::to_string(i_num) << ".config' from scanLog" << std::endl;
            return -1;
        }
        std::string path, type;
        std::string ext[2] = { "before", "after" };
        for (int file_num=0; file_num<2; file_num++) {
            path = i_dir+"/"+cfg_path+"."+ext[file_num];
            type = ext[file_num] + "Cfg";
            json chipCfg = readChipPixCfg(path, m_chiptype);
            if (!chipCfg["is_null"]) {
                /// pixel config to TH2
                chip_name = chipCfg["Name"];
                /// mkdir for chip
                d = this->mkdir(m_file, chip_name);
                /// mkdir for before/after config
                TDirectory* d2 = this->mkdir(d, type);
                d2->cd();
                for (auto& data : chipCfg["PixelConfig"].items()) {
                    json plotData = chipCfg["PixelConfig"][data.key()];
                    this->writeChipPlot(chip_name, plotData, d2);
                }
                /// global config to TEnv
                Env* e = new Env(type, path);
                d2->cd();
                e->write("Global");
                delete e;
            }
        }
    } else {
        d=this->mkdir(m_file, chip_name);
    }
    m_chips[i_num]["name"] = chip_name;
    return 0;
}

int RootFile::writeChipData(std::string i_dir, int i_num) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipData(" << i_dir << ", " << i_num << ")" << std::endl;
#endif
    if (m_chips[i_num]["enable"].is_null()||m_chips[i_num]["enable"]!=0) {
        std::string chip_name;
        if (!m_chips[i_num]["name"].is_null()) {
            chip_name = m_chips[i_num]["name"];
        } else {
            std::cerr << "[ERROR] Could not read 'name' from m_chips. " << std::to_string(i_num) << std::endl;
            return -1;
        }
        /// mkdir for chip
        TDirectory* d = this->mkdir(m_file, chip_name);
        /// chip data
        glob_t globbuf;
        glob((i_dir+"/"+chip_name+"*").c_str(), 0, NULL, &globbuf);
        for (int file_num=0; file_num<globbuf.gl_pathc; file_num++) {
            std::string path = globbuf.gl_pathv[file_num];
            json plotData = readDataFile(path, chip_name);
            this->writeChipPlot(chip_name, plotData, d);
        }
    }
    return 0;
}

int RootFile::loadScanLog() {
#ifdef DEBUG
    std::cout << "RootFile::loadScanLog()" << std::endl;
#endif
    if (!m_infile->Get("scanLog")) {
        std::cerr << "[ERROR] Could not find 'scanLog' Object" << std::endl;
        return -1;
    }
    Env* e = new Env("scanLog", (TEnv*)m_infile->Get("scanLog"));
    json j = e->getJson();
    if (!j["testType"].is_null()) {
        m_testtype = j["testType"];
    } else {
        std::cerr << "[ERROR] Could not read 'testType' from TEnv scanLog" << std::endl;
        return -1;
    }
    if (!j["chipType"].is_null()) {
        m_chiptype = j["chipType"];
    } else {
        std::cerr << "[ERROR] Could not read 'chipType' from TEnv scanLog" << std::endl;
        return -1;
    }
    m_file->cd();
    e->write();
    delete e;
    return 0;
}

int RootFile::loadScanCfg() {
#ifdef DEBUG
    std::cout << "RootFile::loadScanCfg()" << std::endl;
#endif
    if (!m_infile->Get("scanCfg")) {
        std::cerr << "[ERROR] Could not find 'scanCfg' Object" << std::endl;
        return -1;
    }
    Env* e = new Env("scanCfg", (TEnv*)m_infile->Get("scanCfg"));
    m_file->cd();
    e->write();
    delete e;
    return 0;
}

int RootFile::loadChipData() {
#ifdef DEBUG
    std::cout << "RootFile::loadChipData()" << std::endl;
#endif
    for (auto iterator_f : *m_infile->GetListOfKeys()) {
        std::string chip_name = iterator_f->GetName();
        if (!m_infile->Get(chip_name.c_str())->IsFolder()) continue;
        /// mkdir for chip
        TDirectory* d = this->mkdir(m_file, chip_name);
        for (auto itr_d : *m_infile->GetDirectory(chip_name.c_str())->GetListOfKeys()) {
            std::string obj_name = itr_d->GetName();
            if (obj_name=="beforeCfg"||obj_name=="afterCfg") { /// chip config
                /// mkdir for beforeCfg/afterCfg
                TDirectory* d2 = this->mkdir(d, obj_name);
                for (auto itr_o : *m_infile->GetDirectory(chip_name.c_str())->GetDirectory(obj_name.c_str())->GetListOfKeys()) {
                    TObject* o =  m_infile->GetDirectory(chip_name.c_str())->GetDirectory(obj_name.c_str())->Get(itr_o->GetName());
                    std::string data_name = o->GetName();
                    if (data_name=="TEnv") { /// global config
                        Env* e = new Env(data_name, (TEnv*)o);
                        d2->cd();
                        e->write("Global");
                        delete e;
                    } else { /// pixel config
                        /// mkdir for plot
                        TDirectory* d3 = this->mkdir(d2, data_name);
                        d3->cd();
                        this->writeChipPlot(chip_name, o, d3);
                        /// projection
                        this->writeChipPlotProjection(chip_name, o, d3);
                    }
                }
            } else { /// plots
                TObject* o = m_infile->GetDirectory(chip_name.c_str())->Get(obj_name.c_str());
                std::string data_name = obj_name;
                /// mkdir for plot
                TDirectory* d2 = this->mkdir(d, data_name);
                d2->cd();
                this->writeChipPlot(chip_name, o, d2);

                std::string axisX = ((TH1*)o)->GetXaxis()->GetTitle();
                std::string axisY = ((TH1*)o)->GetYaxis()->GetTitle();
                if ((axisX!="Column"&&axisX!="Col")||(axisY!="Row"&&axisY!="Rows")) continue;

                /// projection
                this->writeChipPlotProjection(chip_name, o, d2);

                /// occupancy
                if (data_name.find("OccupancyMap") != std::string::npos )
                    this->writeChipPlotOccupancy(chip_name, o, d2);

                /// Noise Occupancy
                if (data_name.find("NoiseOccupancy") != std::string::npos )
                    this->writeChipPlotNoiseOccupancy(chip_name, o, d2);

                /// Rms
                if ( data_name.find("MeanTotMap")   != std::string::npos ||
                     data_name.find("SigmaTotMap")  != std::string::npos ||
                     data_name.find("ThresholdMap") != std::string::npos ||
                     data_name.find("NoiseMap")     != std::string::npos )
                    this->writeChipPlotRms(chip_name, o, d2);

                /// Gaus
                if ( data_name.find("ThresholdMap") != std::string::npos ||
                     data_name.find("NoiseMap")     != std::string::npos )
                    this->writeChipPlotGaus(chip_name, o, d2);

                delete m_h;
            }
        }
    }
    return 0;
}

int RootFile::getChips() {
#ifdef DEBUG
    std::cout << "RootFile::getChips()" << std::endl;
#endif
    return (int)m_chips.size();
}

///////////////
/// Private ///
///////////////

void RootFile::writeChipPlot(std::string i_name, json& i_data, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlot(" << i_name << ", i_data, i_d)" << std::endl;
#endif
    if ((!i_data["is_null"].is_null()&&i_data["is_null"])||i_data["Type"].is_null()||i_data["Name"].is_null()) return;

    PlotTool* pH;
    if      (i_data["Type"]=="Histo1d") pH = new Histo1D();
    else if (i_data["Type"]=="Histo2d") pH = new Histo2D();
    else                                return;

    pH->setChip(m_chiptype, i_name);
    pH->setData(i_data, i_data["Name"]);
    pH->build();
    i_d->cd();
    pH->write();
    delete pH;
}

void RootFile::writeChipPlot(std::string i_name, TObject* i_o, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlot(" << i_name << ", i_o, i_d)" << std::endl;
#endif
    std::string data_type = i_o->ClassName();
    PlotTool* pH;
    if      (data_type=="TH1F") pH = new Histo1D();
    else if (data_type=="TH2F") pH = new Histo2D();
    else                        return;

    pH->setChip(m_chiptype, i_name);
    i_d->cd();
    if (data_type=="TH1F") {
        pH->setParameters((TH1*)i_o);
        pH->write((TH1*)i_o);
    } else if (data_type=="TH2F") {
        pH->setParameters((TH2*)i_o);
        pH->write((TH2*)i_o);
    }
    pH->print(m_dir, m_print, m_ext);
    delete pH;
}

void RootFile::writeChipPlotProjection(std::string i_name, TObject* i_o, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlotProjection(" << i_name << ", i_o, i_d)" << std::endl;
#endif
    HistoProjection* pP = new HistoProjection();
    pP->setChip(m_chiptype, i_name);
    pP->setData((TH2*)i_o, i_o->GetName(), "Projection");
    pP->build();
    i_d->cd();
    pP->write();
    pP->print(m_dir, m_print, m_ext);
    m_h=pP->getTH();
    delete pP;
}

void RootFile::writeChipPlotOccupancy(std::string i_name, TObject* i_o, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlotOccupancy(" << i_name << ", i_o, i_d)" << std::endl;
#endif
    HistoOccupancy* pO = new HistoOccupancy();
    pO->setChip(m_chiptype, i_name);
    pO->setData((TH2*)i_o, i_o->GetName(), "Occupancy");
    pO->build();
    i_d->cd();
    pO->write();
    pO->print(m_dir, m_print, m_ext);
    delete pO;
}

void RootFile::writeChipPlotNoiseOccupancy(std::string i_name, TObject* i_o, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlotNoiseOccupancy(" << i_name << ", i_o, i_d)" << std::endl;
#endif
    HistoNoiseOccupancy* pN = new HistoNoiseOccupancy();
    pN->setChip(m_chiptype, i_name);
    pN->setData((TH2*)i_o, i_o->GetName(), "NoiseOccupancy");
    pN->build();
    i_d->cd();
    pN->write();
    pN->print(m_dir, m_print, m_ext);
    delete pN;
}

void RootFile::writeChipPlotRms(std::string i_name, TObject* i_o, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlotRms(" << i_name << ", i_o, i_d)" << std::endl;
#endif
    HistoRms* pR = new HistoRms();
    pR->setChip(m_chiptype, i_name);
    pR->setData((TH2*)i_o, i_o->GetName(), "Rms");
    pR->setHisto((TH1*)m_h->Clone());
    pR->build();
    i_d->cd();
    pR->write();
    pR->print(m_dir, m_print, m_ext);
    delete pR;
}
void RootFile::writeChipPlotGaus(std::string i_name, TObject* i_o, TDirectory* i_d) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipPlotGaus(" << i_name << ", i_o, i_d)" << std::endl;
#endif
    HistoGaus* pG = new HistoGaus();
    pG->setChip(m_chiptype, i_name);
    pG->setData((TH2*)i_o, i_o->GetName(), "Gaus");
    pG->fit((TH1*)m_h->Clone());
    pG->build();
    i_d->cd();
    pG->write();
    pG->print(m_dir, m_print, m_ext);
    delete pG;
}
TDirectory* RootFile::mkdir(TFile* i_mother, std::string i_name) {
#ifdef DEBUG
    std::cout << "RootFile::mkdir(i_mother, " << i_name << ")" << std::endl;
#endif
    TDirectory* d;
    if (!i_mother->Get(i_name.c_str())) d = i_mother->mkdir(i_name.c_str());
    else d = (TDirectory*)i_mother->Get(i_name.c_str());
    return d;
}
TDirectory* RootFile::mkdir(TDirectory* i_mother, std::string i_name) {
#ifdef DEBUG
    std::cout << "RootFile::mkdir(i_mother, " << i_name << ")" << std::endl;
#endif
    TDirectory* d;
    if (!i_mother->Get(i_name.c_str())) d = i_mother->mkdir(i_name.c_str());
    else d = (TDirectory*)i_mother->Get(i_name.c_str());
    return d;
}
