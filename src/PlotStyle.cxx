#include "PlotStyle.h"

TStyle* PlotStyle()
{
  TStyle *Style = new TStyle("Plot","Plot style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  Style->SetFrameBorderMode(icol);
  Style->SetFrameFillColor(icol);
  Style->SetCanvasBorderMode(icol);
  Style->SetCanvasColor(icol);
  Style->SetPadBorderMode(icol);
  Style->SetPadColor(icol);
  Style->SetStatColor(icol);
  //Style->SetFillColor(icol); // don't use: white fill color for *all* objects

  //Legend
  Int_t ileg=0;
  Style->SetLegendBorderSize(ileg);
  Style->SetLegendFillColor(ileg);
  Style->SetLegendTextSize(0.045);
  Style->SetLegendFont(42);

  // set the paper & margin sizes
  Style->SetPaperSize(20,26);

  // set margin sizes
  Style->SetPadTopMargin(0.07);
  Style->SetPadRightMargin(0.05);
  Style->SetPadBottomMargin(0.16);
  Style->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  Style->SetTitleXOffset(1.5);
  Style->SetTitleYOffset(1.5);

  // set label offset
  Style->SetLabelOffset(0.01,"xyz");

  // use large fonts
  Int_t font=42; //  helvetica-medium-r-normal "Arial"
  Double_t tsize=0.055;
  Style->SetTextFont(font);

  Style->SetTextSize(tsize);
  Style->SetLabelFont(font,"x");
  Style->SetTitleFont(font,"x");
  Style->SetLabelFont(font,"y");
  Style->SetTitleFont(font,"y");
  Style->SetLabelFont(font,"z");
  Style->SetTitleFont(font,"z");

  Style->SetLabelSize(tsize,"x");
  Style->SetTitleSize(tsize,"x");
  Style->SetLabelSize(tsize,"y");
  Style->SetTitleSize(tsize,"y");
  Style->SetLabelSize(tsize,"z");
  Style->SetTitleSize(tsize,"z");

  // use bold lines and markers
  Style->SetMarkerStyle(20);
  Style->SetMarkerSize(2);
  Style->SetHistLineWidth(2.);
  Style->SetLineStyleString(2,"[12 12]"); // postscript dashes
  Style->SetMarkerColor(kAzure+2);
  Style->SetLineColor(kAzure+2);

  // get rid of X error bars (as recommended in ATLAS figure guidelines)
  Style->SetErrorX(0.0001);
  // get rid of error bar caps
  Style->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  Style->SetOptTitle(0);
  //Style->SetOptStat(1111);
  Style->SetOptStat(0);
  //Style->SetOptFit(1111);
  Style->SetOptFit(0);

  // put tick marks on top and RHS of plots
  Style->SetPadTickX(1);
  Style->SetPadTickY(1);

  return Style;
}

void SetPlotStyle()
{
  TStyle* plotStyle = 0;
  std::cout << "\n\033[34mApplying Plot plotting style settings...\033[m" << std::endl ;
  if ( plotStyle==0 ) plotStyle = PlotStyle();
  gROOT->SetStyle("Plot");
  gROOT->ForceStyle();
  gROOT->SetBatch(true);
  gStyle->SetTickLength(0.02);
  gErrorIgnoreLevel = kWarning;
}
