#include "HistoNoiseOccupancy.h"

///////////////////////////////////////////////////////////////////////////
/// Class for projection from map
HistoNoiseOccupancy::HistoNoiseOccupancy() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::HistoNoiseOccupancy()" << std::endl;
#endif
};

HistoNoiseOccupancy::~HistoNoiseOccupancy(){
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::~HistoNoiseOccupancy()" << std::endl;
#endif
};

void HistoNoiseOccupancy::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.nbins = 9;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 9;

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = 0;
    m_axis.histo.y.high  = 1;

    m_axis.histo.x.title = "Noise Occupancy";
    m_axis.histo.y.title = "Number of Pixels";
    m_axis.histo.z.title = "";

    std::string label[9] = { "<1e-7", "1e-7<", "1e-6<", "1e-5<", "1e-4<", "1e-3<", "1e-2<", "1e-1<", "1<"};
    for (int i=0; i<9; i++) {
        m_axis.histo.x.label.push_back(label[i]);
    }
    m_thr = 1e-6;
}

void HistoNoiseOccupancy::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::fillHisto(json)" << std::endl;
#endif
    int nbinsx = j["x"]["Bins"];
    int nbinsy = j["y"]["Bins"];
    m_zeros = 0;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = j["Data"][col][row];
            if (tmp>1)
                m_h->Fill(8);
            else if (tmp>1e-1)
                m_h->Fill(7);
            else if (tmp>1e-2)
                m_h->Fill(6);
            else if (tmp>1e-3)
                m_h->Fill(5);
            else if (tmp>1e-4)
                m_h->Fill(4);
            else if (tmp>1e-5)
                m_h->Fill(3);
            else if (tmp>1e-6)
                m_h->Fill(2);
            else if (tmp>1e-7)
                m_h->Fill(1);
            else
                m_h->Fill(0);
        }
    }
}
void HistoNoiseOccupancy::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    m_zeros = 0;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = h->GetBinContent(col+1, row+1);
            if (tmp>1)
                m_h->Fill(8);
            else if (tmp>1e-1)
                m_h->Fill(7);
            else if (tmp>1e-2)
                m_h->Fill(6);
            else if (tmp>1e-3)
                m_h->Fill(5);
            else if (tmp>1e-4)
                m_h->Fill(4);
            else if (tmp>1e-5)
                m_h->Fill(3);
            else if (tmp>1e-6)
                m_h->Fill(2);
            else if (tmp>1e-7)
                m_h->Fill(1);
            else
                m_h->Fill(0);
        }
    }
};

void HistoNoiseOccupancy::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    m_h->Draw("TEXT0 SAME");

    //m_c->SetLogx();
    //m_c->SetLogy();

    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.5)); //Leave extra room for legend
    m_c->Update();
};

json HistoNoiseOccupancy::getResult() {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::getResult()" << std::endl;
#endif
    json result = Histo1D::getResult();
    result["noiocc"] = m_h->GetBinContent(1)+m_h->GetBinContent(2)+m_h->GetBinContent(3);

    return result;
}
