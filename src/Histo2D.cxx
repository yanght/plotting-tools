#include "Histo2D.h"

///////////////////////////////////////////////////////////////////////////
/// Class for 2D histogram

Histo2D::Histo2D() {
#ifdef DEBUG
    std::cout << "Histo2D::Histo2D()" << std::endl;
#endif
    //m_latex_pos = { 0.23,0.96,0.72,0.96 };
    m_latex_pos = { 0.2,0.96,0.8,0.96,0.5,0.92 };
};

Histo2D::~Histo2D() {
#ifdef DEBUG
    std::cout << "Histo2D::~Histo2D()" << std::endl;
#endif
};

void Histo2D::buildHisto() {
#ifdef DEBUG
    std::cout << "Histo2D::buildHisto()" << std::endl;
#endif
    std::string histo_name;
    if (m_type=="") histo_name = m_data_name;
    else            histo_name = m_type + "Map";
    std::replace(histo_name.begin(), histo_name.end(), '-', '_');

    int nbinsx             = m_axis.histo.x.nbins;
    float xlow             = m_axis.histo.x.low;
    float xhigh            = m_axis.histo.x.high;
    int nbinsy             = m_axis.histo.y.nbins;
    float ylow             = m_axis.histo.y.low;
    float yhigh            = m_axis.histo.y.high;
    std::string xaxistitle = m_axis.histo.x.title;
    std::string yaxistitle = m_axis.histo.y.title;
    std::string zaxistitle = m_axis.histo.z.title;

    m_h = new TH2F(histo_name.c_str(), "", nbinsx, xlow, xhigh, nbinsy, ylow, yhigh);
    style_TH2((TH2*)m_h, xaxistitle.c_str(), yaxistitle.c_str(), zaxistitle.c_str());

    if (m_axis.histo.x.label.size()!=0) {
        for (int i=0; i<(int)m_axis.histo.x.label.size(); i++) m_h->GetXaxis()->SetBinLabel(i+1, m_axis.histo.x.label[i].c_str());
        m_h->GetXaxis()->LabelsOption("h");
        m_h->SetMarkerSize(1.8);
        m_h->SetMarkerColor(1);
    }
    if (m_axis.histo.y.label.size()!=0) {
        for (int i=0; i<(int)m_axis.histo.y.label.size(); i++) m_h->GetYaxis()->SetBinLabel(i+1, m_axis.histo.y.label[i].c_str());
        m_h->GetYaxis()->LabelsOption("h");
        m_h->SetMarkerSize(1.8);
        m_h->SetMarkerColor(1);
    }
};

void Histo2D::setCanvas() {
#ifdef DEBUG
    std::cout << "Histo2D::setCanvas()" << std::endl;
#endif
    m_c = new TCanvas(Form("canv_%s",m_h->GetName()), "", 800, 600);
    style_TH2canvas(m_c);
};

void Histo2D::drawHisto() {
#ifdef DEBUG
    std::cout << "Histo2D::drawHisto()" << std::endl;
#endif
    style_TH2((TH2*)m_h, m_h->GetXaxis()->GetTitle(), m_h->GetYaxis()->GetTitle(), m_h->GetZaxis()->GetTitle());
    if (m_axis.overwrite) {
        m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
        m_h->GetYaxis()->SetRangeUser(m_axis.histo.y.low, m_axis.histo.y.high);
        m_h->GetZaxis()->SetRangeUser(m_axis.histo.z.low, m_axis.histo.z.high);
    }
    m_h->Draw("colz");
};

void Histo2D::setParameters(json& j) {
#ifdef DEBUG
    std::cout << "Histo2D::setParameters(json)" << std::endl;
#endif
    PlotTool::setParameters(j);
}
void Histo2D::setParameters(TH1* h) {
#ifdef DEBUG
    std::cout << "Histo2D::setParameters(TH1)" << std::endl;
#endif
    PlotTool::setParameters(h);
}
void Histo2D::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "Histo2D::setParameters(TH2)" << std::endl;
#endif
    PlotTool::setParameters(h);
}

void Histo2D::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "Histo2D::fillHisto(j)" << std::endl;
#endif
    int nbinsx = j["x"]["Bins"];
    int nbinsy = j["y"]["Bins"];
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = j["Data"][col][row];
            m_h->SetBinContent(col+1, row+1, tmp);
        }
    }
};
void Histo2D::fillHisto(TH1* h) {
#ifdef DEBUG
    std::cout << "Histo2D::fillHisto(TH1)" << std::endl;
#endif
};
void Histo2D::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "Histo2D::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = h->GetBinContent(col+1, row+1);
            double par_x = h->ProjectionX()->GetBinCenter(col+1);
            double par_y = h->ProjectionY()->GetBinCenter(row+1);
            m_h->SetBinContent(m_h->FindBin(par_x, par_y), tmp);
        }
    }
};

json Histo2D::getResult() {
#ifdef DEBUG
    std::cout << "Histo2D::getResult()" << std::endl;
#endif
    json result;
    result["entry"] = m_h->GetEntries();

    return result;
}
