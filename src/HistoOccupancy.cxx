#include "HistoOccupancy.h"

///////////////////////////////////////////////////////////////////////////
/// Class for projection from map
HistoOccupancy::HistoOccupancy() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoOccupancy::HistoOccupancy()" << std::endl;
#endif
};

HistoOccupancy::~HistoOccupancy(){
#ifdef DEBUG
    std::cout << "HistoOccupancy::~HistoOccupancy()" << std::endl;
#endif
};

void HistoOccupancy::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoOccupancy::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.nbins = 6;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 6;

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = 0;
    m_axis.histo.y.high  = 1;

    m_axis.histo.x.title = "Occupancy [%]";
    m_axis.histo.y.title = "Number of Pixels";
    m_axis.histo.z.title = "";

    std::string label[6] = { "0%", " 0-98%", " 98-100%", "100%", "100-102%", " >102%"};
    for (int i=0; i<6; i++) {
        m_axis.histo.x.label.push_back(label[i]);
    }
    m_thr = 100;
}

void HistoOccupancy::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "HistoOccupancy::fillHisto(json)" << std::endl;
#endif
    int nbinsx = j["x"]["Bins"];
    int nbinsy = j["y"]["Bins"];
    m_zeros = 0;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = j["Data"][col][row];
            int bin_num = whichBin(m_thr, tmp);
            //m_h->AddBinContent(bin_num);
            m_h->Fill(bin_num-1);
        }
    }
}
void HistoOccupancy::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoOccupancy::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    m_zeros = 0;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = h->GetBinContent(col+1, row+1);
            int bin_num = whichBin(m_thr, tmp);
            //m_h->AddBinContent(bin_num);
            m_h->Fill(bin_num-1);
        }
    }
};

void HistoOccupancy::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoOccupancy::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    m_h->Draw("TEXT0 SAME");

    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.5)); //Leave extra room for legend

    m_c->Update();
};

json HistoOccupancy::getResult() {
#ifdef DEBUG
    std::cout << "HistoOccupancy::getResult()" << std::endl;
#endif
    json result = Histo1D::getResult();
    result["occ"] = m_h->GetBinContent(4);

    return result;
}
