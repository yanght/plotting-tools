#include "HistoProjection.h"

///////////////////////////////////////////////////////////////////////////
/// Class for projection from map
HistoProjection::HistoProjection() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoProjection::HistoProjection()" << std::endl;
#endif
};

HistoProjection::~HistoProjection(){
#ifdef DEBUG
    std::cout << "HistoProjection::~HistoProjection()" << std::endl;
#endif
};

void HistoProjection::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoProjection::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.low   = h->GetBinContent(h->GetMinimumBin())-0.5;
    m_axis.histo.x.high  = h->GetBinContent(h->GetMaximumBin())+0.5;
    if ((m_axis.histo.x.high-m_axis.histo.x.low)==1) {
        m_axis.histo.x.nbins = 3;
    } else if ((m_axis.histo.x.high-m_axis.histo.x.low)==std::floor(m_axis.histo.x.high-m_axis.histo.x.low)) {
        m_axis.histo.x.nbins = (int)(m_axis.histo.x.high-m_axis.histo.x.low)/1;
    } else {
        m_axis.histo.x.nbins = 100;
    }

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = 0;
    m_axis.histo.y.high  = 1;

    m_axis.histo.x.title = h->GetZaxis()->GetTitle();
    m_axis.histo.y.title = "Number of Pixels";
    m_axis.histo.z.title = "";
}

void HistoProjection::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "HistoProjection::fillHisto(json)" << std::endl;
#endif
    int nbinsx = j["x"]["Bins"];
    int nbinsy = j["y"]["Bins"];
    m_zeros = 0;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = j["Data"][col][row];
            if (tmp!=0) m_h->Fill(tmp);
            if (tmp==0) m_zeros++;
        }
    }
};
void HistoProjection::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoProjection::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    m_zeros = 0;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = h->GetBinContent(col+1, row+1);
            if (tmp!=0) m_h->Fill(tmp);
            if (tmp==0) m_zeros++;
        }
    }
};

void HistoProjection::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoProjection::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    //
    //if (m_data_type=="occ"||m_data_type=="noise_occ") {
    //    m_h->Draw("TEXT0 SAME");
    //}
    m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
    if (m_axis.overwrite) {
        double mean = m_h->GetMean();
        double rms = m_h->GetRMS();
        double min = (mean-5*rms<0)?-0.5:(mean-5*rms);
        double max = (mean+5*rms);
        m_h->GetXaxis()->SetRangeUser(min, max);
    }
    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.5)); //Leave extra room for legend
    m_c->Update();
};

json HistoProjection::getResult() {
#ifdef DEBUG
    std::cout << "HistoProjection::getResult()" << std::endl;
#endif
    json result = Histo1D::getResult();

    return result;
}
